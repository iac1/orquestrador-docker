#!/bin/bash

PATH_WWW="/var/www"
PATH_DEPLOYMENT="/tmp"
PATH_TRANSITION="/tmp/PATH_TRANSITION"
DIR_PERMISSION=$PATH_TRANSITION
HOME_USER="/root"
ENVIRONMENT="$HOME_USER/libs/scripts-in-container/environment"
BRANCH=""
RUN_COMPOSER=""
APP_LARAVEL=""
CURRENT_COMMIT=""
LAST_COMMIT=""
DATE_TIME="$(date +'%d %B %Y, %A às %H:%M:%S')"
THIS_SCRIPT_NAME=$(basename "$0")
DIR_GITLAB_PULL="$HOME_USER/libs/scripts-in-container"
GITLAB_PULL="$THIS_SCRIPT_NAME"
MAX_PROCESS=2
TOTAL_PROCESS_GITLAB_PULL=""

# Cria diretório de transição se não existir
# O diretório de transição é importante para evitar que a aplicação pare enquanto houver atualização (Deployment Continuous)
if [ ! -d $PATH_TRANSITION ];then
    mkdir $PATH_TRANSITION > /dev/null 2>&1
fi

# Inclusão de funções
source $HOME_USER/libs/scripts-in-container/libs/setAlert
source $HOME_USER/libs/scripts-in-container/libs/getVariable
source $HOME_USER/libs/scripts-in-container/libs/dirPermission
# Define variável
source $HOME_USER/libs/scripts-in-container/libs/defineVariable

# Altera para diretório de fluxo de trabalho (PATH_DEPLOYMENT). Este diretório é apenas uma ponte para o diretório oficial "/var/www".
# O diretório de fluxo de trabalho é importante por evita que o pull fique 'travado' se existir algum arquivo modificado
# esperando ser commitado.
# No diretório de fluxo trabalho numca haverá arquivos esperando ser commitados!
cd $PATH_DEPLOYMENT/$CONTAINER_NAME

# TOTAL_PROCESS_GITLAB_PULL=$(ps aux | grep "$DIR_GITLAB_PULL/$GITLAB_PULL" | wc -l)
# # Se este script já/ainda estiver  em execução é terminado para não duplicar o processo.
# if [ $TOTAL_PROCESS_GITLAB_PULL -gt $MAX_PROCESS ];then
#   setAlert "warning" "Já existe um processo em execução."
#   exit
# fi

# Checagem e comparação dos commits remoto e local
CURRENT_COMMIT=$(git log $BRANCH --max-count=1 | awk '{print $2}' | head --line=1)
LAST_COMMIT=$(git ls-remote origin $BRANCH | awk '{print $1}')
if [ ${#LAST_COMMIT} -eq 0 ];then
  setAlert "fatal" "Não pode obter último commit do servidor GIT remoto"
  exit
fi

# Verificação de update na branch.
# VERIFY_UPDATE=$(git pull origin $BRANCH | grep -e "Already up to date" -e "Already up-to-date" > /dev/null 2>&1 ; echo $?)
if [ "$CURRENT_COMMIT" != "$LAST_COMMIT" ]; then
    git pull origin $BRANCH > /dev/null 2>&1
    if [ $? -ne 0 ];then
      setAlert "fatal" "Falha ao atualizar a branch (git pull origin $BRANCH)"
      exit
    fi
    # Sincroniza o diretório de fluxo de trabalho com diretório de transição
    rsync -a --delete $PATH_DEPLOYMENT/$CONTAINER_NAME/ $PATH_TRANSITION
    # Entra no diretório de transição para execução de tarefas de rotinas
    cd $PATH_TRANSITION

    setAlert "success" "Ajustando permissões em $PATH_TRANSITION"
    # Configura permissões para dono e grupo
    chown -R root:www-data $PATH_TRANSITION
    # Permissão geral de para dono, grupo e outros em WWW:
    chmod -R 744 $PATH_TRANSITION
    # Reconfigura as permissões apenas de grupo para diretórios (acesso Apache - www-data):
    find $PATH_TRANSITION -type d -exec chmod g=rx {} ";"
    # Reconfigura permissões apenas para diretórios contidos no array PERMISSION que necessitam de leitura e escrita (storage, uploads e etc.):
    for i in ${PERMISSION[@]}
    do
      chmod -R $PERMISSION_VALUE $i > /dev/null 2>&1
    done

    # Executa pipeline após o pull do projeto orientando-se por meio do arquivo de orquestração orq-pipeline[ PROD, DEV, STAGE ].yml
    python $HOME_USER/libs/scripts-in-container/pipeline/pipeline.py "$PATH_TRANSITION/"

    # Sincroniza o diretório de transição com diretório oficial preservando permissões e grupos
    rsync -a $PATH_TRANSITION/ $PATH_WWW

    echo "Update da branch $BRANCH - $(date)" >> $PATH_LOG
fi
