# Verifica se Apache está instalado
if [ ! -d $DIR_APACHE2 ]; then
  setAlert "fatal" "Diretório [$DIR_APACHE2] não encontrado. Verifique instalação do Apache2 (apt-get install apache2)."
  read -p "Deseja instalar o Apache2 agora e prosseguir? (y/n): " yn
  if [ "$yn" == "y" ];then
    apt-get update && apt-get install -y apache2
  else
    exit
  fi

fi

# Cria domínio e sub-domínos conforme array informado nas configurações do usuário
for DOMAIN in ${VIRTUALHOST_DOMAIN[@]}
do

    setAlert "success" "Lincando Docker na porta $MAPPER_PORT_EXTERNAL com Apache2 do hospedeiro para domínio $DOMAIN"

    NEW_FILE_NAME="$DOMAIN.conf"

    if [ -z $DOMAIN ];then
      setAlert "fatal" "Variável VIRTUALHOST_DOMAIN não definida!"
      setAlert "warning" "Verifique as configurações de variáveis em: [ $appFileConf ]"
      exit
    fi

    cp $DIR_MODEL/$FILE_NAME_MODEL $DIR_SWAP/
    sed -i "s/VIRTUALHOST_DOMAIN/$DOMAIN/g" $DIR_SWAP/$FILE_NAME_MODEL
    sed -i "s/PORT_DOCKER/$MAPPER_PORT_EXTERNAL/g" $DIR_SWAP/$FILE_NAME_MODEL


    # Deleta qualquer virtualhost referente ao nome do projeto processado para criação dinâmica de um novo, conforme especificado no
    # arquivo de configuração.
    rm -f $DIR_APACHE2/$DOMAIN*.conf > /dev/null 2>&1

    # Verifica se aplição necessita de redirecionamento para HTTPS
    if [ $FORCE_HTTPS == "true" ];then
      setAlert "success" "Configurando redirecionamento automático para HTTPS..."
      echo "" >> $DIR_SWAP/$FILE_NAME_MODEL
      echo "RewriteEngine on" >> $DIR_SWAP/$FILE_NAME_MODEL
      echo "RewriteCond %{SERVER_NAME} =$DOMAIN" >> $DIR_SWAP/$FILE_NAME_MODEL
      echo "RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent] " >> $DIR_SWAP/$FILE_NAME_MODEL
    fi

    # Fecha o arquivo modelo
    echo "" >> $DIR_SWAP/$FILE_NAME_MODEL
    echo "</VirtualHost>" >> $DIR_SWAP/$FILE_NAME_MODEL

    # Move o arquivo configurado para diretório do Apache [/etc/apache2/sites-enabled]
    mv $DIR_SWAP/$FILE_NAME_MODEL $DIR_APACHE2/$NEW_FILE_NAME

    # Reload Apache2
    /etc/init.d/apache2 reload > /dev/null 2>&1

    # Instala o certificado SSL Let's Encrypt
    if [ $CREATE_CERT_LETS_ENCRYPT == "true" ];then
      setAlert "success" "Gerando certificado SSL para $DOMAIN"
      $RUN_CERTBOT $DOMAIN > /dev/null 2>&1
      if [ $? -ne 0 ];then
          setAlert "danger" "Algo deu errado ao gerar o certificado Let's Encrypt para o domínio [$DOMAIN]"
          setAlert "warning" "1) Verifique se Certbot se encontra instalado (https://certbot.eff.org/lets-encrypt/ubuntuother-apache)."
          setAlert "warning" "2) Verifique o apontamento de DNS do domínio $DOMAIN para o IP deste servidor"
          setAlert "warning" "3) Experimente executar o seguinte comando manualmente:"
          setAlert "warning" "$RUN_CERTBOT $DOMAIN"
      fi
    fi
    /etc/init.d/apache2 reload > /dev/null 2>&1
    if [ $? -ne 0 ];then
      setAlert "danger" "Algo deu errado ao lincar o Apache para [ $DOMAIN ]"
      exit
    fi
done

# Cria link simbólico para virtualhost do Orq-Pipeline
ln -fs $DIR_MODEL/000-default.conf $DIR_APACHE2/000-default.conf
# habilita módulos para apache2
a2enmod headers proxy proxy_http rewrite > /dev/null 2>&1
# Reload Apache2
/etc/init.d/apache2 reload > /dev/null 2>&1

#setAlert "success" "Fim do link de Docker com Apache2."
echo -e "Resumo do provisionamento para $CONTAINER_NAME:"
echo -e "\t - Nome do container: $PREFIX_NAME$CONTAINER_NAME"
echo -e "\t - ID do container: $CONTAINER_ID"
echo -e "\t - Domínio e portas da aplicação:"
for DOMAIN in ${VIRTUALHOST_DOMAIN[@]}
do
  echo -e "\t\t - URL de acesso direto: http://$DOMAIN:$MAPPER_PORT_EXTERNAL"
  echo -e "\t\t - URL de acesso pelo proxy: http://$DOMAIN"
done
echo -e "\t - Redirecionamento para HTTPS: $FORCE_HTTPS"
echo -e "\t - Certificado SSL: $CREATE_CERT_LETS_ENCRYPT"
echo -e "\t - Repositório GIT: $GIT_REPOSITORY"
echo -e "\t - Branch: $BRANCH"
echo -e "\t - Imagem Docker: $IMAGE_NAME"
