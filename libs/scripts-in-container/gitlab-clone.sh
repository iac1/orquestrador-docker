#!/bin/bash

PATH_WWW="/var/www"
DIR_PERMISSION=$PATH_WWW
PATH_DEPLOYMENT="/tmp"
HOME_USER="/root"
# CONTAINER_NAME=$CONTAINER_NAME
PATH_LOG="/var/log/$CONTAINER_NAME.log"
BRANCH_CURRENT="/tmp/branch_current.log"
# GIT_REPOSITORY=$GIT_REPOSITORY
DINAMIC_NAME=$(date +'%H%M%S%N')
ENVIRONMENT="$HOME_USER/libs/scripts-in-container/environment"

# Inclusão de funções
source $HOME_USER/libs/scripts-in-container/libs/setAlert
source $HOME_USER/libs/scripts-in-container/libs/getVariable
source $HOME_USER/libs/scripts-in-container/libs/dirPermission
# Define variável
source $HOME_USER/libs/scripts-in-container/libs/defineVariable

# Altera para diretório de fluxo de trabalho (PATH_DEPLOYMENT). Este diretório é apenas uma ponte para o diretório oficial "/var/www".
# O diretório de fluxo de trabalho é importante por evita que o pull fique 'travado' se existir algum arquivo modificado
# esperando ser commitado.
# No diretório de fluxo trabalho numca haverá arquivos esperando ser commitados!
cd $PATH_DEPLOYMENT

# Permissão na home do usuário (700, para se adequar as permissões exigidas pelo ssh para uso da chave rsa):
chmod -R 700 $HOME_USER

# Clona o repositório. Se algum erro no clone, o escript é paralizado.
setAlert "success" "Clonando respositório $CONTAINER_NAME..."

# clona o projeto
git clone --quiet $GIT_REPOSITORY $CONTAINER_NAME > /dev/null 2>&1
if [ ! -d ./$CONTAINER_NAME ];then
  setAlert "fatal" "Não pode clonar o repositório [ $GIT_REPOSITORY ]"
  exit
fi

# Entra no diretório do projeto clonado.
cd $PATH_DEPLOYMENT/$CONTAINER_NAME

# Verifica se está na branche atual conforme descrito no arquivo de configuração do projeto app... e executa a troca de branch.
setAlert "success" "Configurando projeto para branch $BRANCH"
git branch | grep -w "*" | awk '{print $2}' > $BRANCH_CURRENT
BRANCH_CURRENT=$(cat $BRANCH_CURRENT)
if [ "$BRANCH_CURRENT" != "$BRANCH" ];then
    # Baixa a branch declarada no arquivo de configuração em app/conf-enabled
    git fetch origin $BRANCH:$BRANCH > /dev/null 2>&1
    [[ $? -eq 0 ]] || setAlert "fatal" "Algo deu errado ao baixar a branch $BRANCH"
    # Traoca para branch declarada no arquivo de...
    git checkout $BRANCH > /dev/null 2>&1
    [[ $? -eq 0 ]] || setAlert "fatal" "Algo deu errado ao trocar para branch $BRANCH"
fi
# Executa configurações Git config
git config core.filemode false

# Exclui o diretório WWW
rm -rf $PATH_WWW/

# Sincroniza o diretório de fluxo de trabalho com diretório oficial
rsync -a $PATH_DEPLOYMENT/$CONTAINER_NAME/ $PATH_WWW

# Entra no diretório oficial da aplicação para execução de tarefas de rotinas
cd $PATH_WWW

setAlert "success" "Configurando permissões em $PATH_WWW para $CONTAINER_NAME..."
# Configura permissões para dono e grupo
chown -R root:www-data $PATH_WWW
# Permissão geral de para dono, grupo e outros em WWW:
chmod -R 744 $PATH_WWW
# Reconfigura as permissões apenas de grupo para diretórios (acesso Apache - www-data):
find $PATH_WWW -type d -exec chmod g=rx {} ";"
# Reconfigura permissões apenas para diretórios contidos no array PERMISSION que necessitam de leitura e escrita (storage, uploads e etc.):
for i in ${PERMISSION[@]}
do
  chmod -R $PERMISSION_VALUE $i > /dev/null 2>&1
done
# Executa pipeline após o clone do projeto orientando-se por meio do arquivo de orquestração orq-pipeline[ PROD, DEV, STAGE ].yml
setAlert "success" "Procurando por instruções de pipeline..."
python $HOME_USER/libs/scripts-in-container/pipeline/pipeline.py "$PATH_WWW/"
