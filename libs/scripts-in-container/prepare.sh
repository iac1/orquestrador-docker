#!/bin/bash
HOME_USER="/root"
PATH_DEPLOYMENT="/tmp"

# Inclusão de funções
source $HOME_USER/libs/scripts-in-container/libs/setAlert
source $HOME_USER/libs/scripts-in-container/libs/installPackeges

# Cria um arquivo com as variáveis de ambiente para uso do cron.
printenv > $HOME_USER/libs/scripts-in-container/environment

# Copia os arquivos cron para cron.d
cp -f $HOME_USER/libs/scripts-in-container/cron.d/* /etc/cron.d/

# Configura usuário git local
git config --global user.email "deployer@localhost"
git config --global user.name "Deployer"

# Executa git-clone
bash $HOME_USER/libs/scripts-in-container/gitlab-clone.sh

# Configura permissões para cron e libs/scripts-in-container
chown -R root:root /etc/cron*
chmod 644 /etc/cron.d/*
chmod -R 755 $HOME_USER/libs/scripts-in-container/

# Inicia o cron
/etc/init.d/cron start > /dev/null 2>&1
if [ $? -ne 0 ];then
  setAlert "danger" "Não pode iniciar o Cron no container [ $CONTAINER_NAME ]"
  exit
fi

# Inicia o servidor Apache2
/etc/init.d/apache2 start > /dev/null 2>&1
if [ $? -ne 0 ];then
  setAlert "danger" "Não pode iniciar o Apache no container [ $CONTAINER_NAME ]"
  exit
fi

#setAlert "success" "Fim dos trabalhos dentro do container $CONTAINER_NAME..."
