## Orq-Pipeline
Orq-Pipeline é uma ferramenta IaC (Infrastructure as a Code) para orquestração e provisionamento de aplicativos em container Docker.
- Provisionamento simples e rápido
- Integraçao com repositório de código
- Deployment Continuous (CD)
- Pipeline de comandos integrados com Slack para report da execução
- Orquestração orientada por arquivo YML (orq-pipeline-[dev,stage,prod].yml) para diferentes ambientes (desenvolvimento, homologação e produção)
- Configuração simplificada de aplicações no formato ENV.

### Requisitos do sistema:
- Sistema operacial: Ubuntu ou Debian.
- Utilitário: Git, cURL

### Tipos de ambiente suportado
O Orq-Pipeline é executado nos três ambientes principais (DEV, STAGE e PROD).

Em cada arquivo descritor de provisionamento existe uma variável chamada TYPE_ENVIRONMENT que determina o tipo ambiente que será provisionada a aplicação. Isso possibilita a execução da aplicação e pipelines conforme ambiente apropriado direcionando corretamente repositório de código e banco de dados para cada aplicação. Exemplo:

Ambiente de DEV:
```TYPE_ENVIRONMENT=DEV``` (dipsara catilho para execuçao do pipeline contido em orq-pipeline-dev.yml)

Ambiente STAGE:
```TYPE_ENVIRONMENT=STAGE``` (dipsara catilho para execuçao do pipeline contido em orq-pipeline-stage.yml)

Ambiente PROD:
```TYPE_ENVIRONMENT=PROD``` (dipsara catilho para execuçao do pipeline contido em orq-pipeline-prod.yml)

### Diagrama geral do Orq-Pipeline
![alt text](./diagrams/diagrama-orquestracao-container-OrqPipeline.jpg)

<a href="![alt text](diagrams/diagrama-orquestracao-container-OrqPipeline.jpg)">Download</a>

### Instalação:


```
# Instalação do Docker
curl -fsSL https://get.docker.com -o get-docker.sh && sh get-docker.sh

# Clone do projeto Orq-Pipeline
cd /opt/
git clone git@[undefined]:iac1/orquestrador-docker.git orquestrador-docker

```


### Cadastrando o primeiro aplicativo:
```
cd /opt/orquestrador-docker/
cp app/conf-enabled/app.conf.exemple app/conf-enabled/meu-app-laravel.conf
# Modifique o arquivo conforme seu projeto:
vim app/conf-enabled/meu-app-laravel.conf
```
Arquivo Exemplo:

```
# Tipo de ambiente (isso define qual arquio de pipeline (orq-pipeline-X.yml) será processado)
TYPE_ENVIRONMENT=DEV
# Nome da aplicação
CONTAINER_NAME=nomedoprojeto
# Repositório GIT
GIT_REPOSITORY=git@gitlab.com:USUARIO/respositorio.git
# Branch do repositório GIT (master, develop e etc.)
BRANCH=develop
# Nome da imagem do repositório (Seu Docker registry)
IMAGE_NAME=registry.gitlab.com/USUARIO/IMAGE:latest
# Nome de domínio DNS da apalicação - Declaração no formato Array VIRTUALHOST_DOMAIN[0], VIRTUALHOST_DOMAIN[1] etc..
VIRTUALHOST_DOMAIN[0]=meudominio.com
# Cria certificado SSL Let's Encrypt.
CREATE_CERT_LETS_ENCRYPT=false
# Habilitar redirecionamento HTTPS
FORCE_HTTPS=false
```

### Executar/Provisionar:
```
bash orquestrar.sh
```


###### Arquitetura e desenvolvimento:

- Leomar Duarte (https://www.linkedin.com/in/leomar-duarte/)
- Marcelo Viana (https://www.linkedin.com/in/marcelovianaalmeida/)
