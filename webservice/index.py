#!/usr/bin/python
# -*- coding: UTF-8 -*-

import flask, os, sys, codecs
from flask import request, jsonify, json
from flask_cors import CORS
from werkzeug import secure_filename

dir_orquestrador = "/opt/orquestrador-docker/"
conf_enabled = dir_orquestrador+"app/conf-enabled/"

app = flask.Flask(__name__)
app.config["DEBUG"] = True
CORS(app)

# def salvarOrquestradorPipeline(dados):
#     nome_container = dados['container_name']
#     pipeline = dados['pipeline']['env']
#     with codecs.open(dir_orquestrador + 'app/orquestrador-pipeline/' + nome_container + '.yml', 'w', encoding='utf8') as arquivo_nome_container:
#         for i in pipeline:
#             arquivo_nome_container.write(i)
#         arquivo_nome_container.close()
#     return True

def confEnabled(dados):
    nome_container = dados['container_name']
    with codecs.open(conf_enabled + nome_container + '.conf', 'w', encoding='utf8') as arquivo_nome_container:
        for i in dados:
            if i != "checked":
                arquivo_nome_container.write(str(i).upper() + '=' + str(dados[i]) + format('\n'))
            else:
                for c in dados[i]:
                    arquivo_nome_container.write(str(c).upper() + '=' + 'true' + format('\n'))
        arquivo_nome_container.close()
    return "Sucesso"
    # if salvarOrquestradorPipeline(dados):
    #     return "Sucesso"

def executaOrquestrar():
    resposta = os.popen('bash '+dir_orquestrador+'orquestrar.sh')
    return str(resposta.read())


def deletaArquivoConfiguracao(nome_container):
    #return conf_enabled + nome_container + '.conf'
    # stop container
    if os.popen('docker stop orq-'+nome_container):
        if os.remove(conf_enabled + nome_container + '.conf'):
            return jsonify(message='Container ' + nome_container + 'deletado.')
    return jsonify(message='Falha ao deletar ' + nome_container)


@app.route('/', methods=['GET'])
def home():
    return jsonify(message='Interface de provisionamento self-service para containers do Orquestrador Docker.')


@app.route('/set/', methods=['POST'])
def apiSet():
    response = request.get_json()
    dados = response['dados']
    if confEnabled(dados):
        return executaOrquestrar()

@app.route('/get/', methods=['GET', 'POST'])
def api_get():
    # response = {
    #             "docker_run": os.popen('docker inspect $(docker ps -q --filter "name=orq-")').read(),
    #             "docker_config_files": {}
    #             }
    #
    getConfigFiles = {}
    configFiles = []
    for x in os.listdir(conf_enabled):
        getConfigFiles['filename'] = x
        # f = open(conf_enabled + '/' + x, "r")
        # getConfigFiles['env'] = f.readlines()
        # f.close()
        with open(conf_enabled + '/' + x, "r") as f:
          getConfigFiles['env'] = []
          for line in f:
            if '#' not in line:
                getConfigFiles['env'].append(line)

        configFiles.append(getConfigFiles)
        getConfigFiles = {}

    #
    # return jsonify(getConfigFiles)
    # # return jsonify([{'filename': 'arquivo1', 'env': ['env1', 'env2']},{'filename': 'arquivo1', 'env': ['env1', 'env2']}])
    #
    # response['docker_run'] = response['docker_run']
    #
    # if not response:
    #     return jsonify(message='Nenhum container em execução.')
    # return response, 200, {'content-type': 'application/json'}
    return jsonify(configFiles)

@app.route('/delete/', methods=['DELETE', 'POST'])
def apiDelete():
    response = request.get_json()
    dados = response['dados']
    nome_container = dados['container_name']
    return deletaArquivoConfiguracao(nome_container)

@app.route('/action/', methods=['POST'])
def apiAction():
    response = request.get_json()
    dados = response['dados']
    return dados

app.run()
