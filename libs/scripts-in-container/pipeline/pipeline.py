# -*- coding: utf-8 -*-
# Executor de configurações de aplicação
# Orquestrador Docker
import yaml
import os
import sys
# Declaração de variáveis e passagem de argumentos via linha de comando
diretorio_home_pipeline = os.path.dirname(os.path.abspath( __file__ ))
args = len(sys.argv) -1
if args:
    dir_app = sys.argv[1]
else:
    dir_app = '/var/www/'

orquestrador_pipeline = ""
# Testa a variável de ambiente
if not os.getenv('TYPE_ENVIRONMENT'):
    print('pipeline não pode executar. Por favor, defina a variável TYPE_ENVIRONMENT em seu arquivo de configuração.')
    sys.exit()

ambiente = os.getenv('TYPE_ENVIRONMENT').upper()

# Identificação do ambiente (desenvolvimento ou produção)
if ambiente == "PROD":
    orquestrador_pipeline = dir_app + "orq-pipeline-prod.yml"
elif ambiente == "STAGE":
    orquestrador_pipeline = dir_app + "orq-pipeline-stage.yml"
else: # DEV
    orquestrador_pipeline = dir_app + "orq-pipeline-dev.yml"

# Verifica se descritor YML para orquestrador existe
if not os.path.exists(orquestrador_pipeline):
    if os.path.exists(dir_app + 'orquestrador-pipeline.yml'):
        orquestrador_pipeline = dir_app + 'orquestrador-pipeline.yml'
    else:
        print('Pipeline: aplicação não possui ' + orquestrador_pipeline)
        sys.exit()

print('Pipeline: identificação do ambiente: '+ ambiente)
print('Pipeline: instruções conforme arquivo: ' + orquestrador_pipeline)

# Navega para diretório da aplicação
os.chdir(dir_app)
print('Pipeline: executando em: [' + os.getcwd() +']')

#  Abre arquivo yml
with open(orquestrador_pipeline,'r') as file:

    ## Converte yml em obj
    arquivo_yml = yaml.load(file, Loader=yaml.FullLoader)

    ## Coleta variáveis
    env = arquivo_yml['env']
    dir_env = arquivo_yml['dir_env']
    executar_comando = arquivo_yml['executar_comando']
    tipo_env = 'ENV'
    for atributo in arquivo_yml:
        if 'tipo_env' == atributo:
            tipo_env = arquivo_yml['tipo_env'].upper()

    print("Pipeline: tipo de env: " +tipo_env)

    ## criação de arquivos de banco de dados para :
    # converte array para yaml se projeto for symfony
    if tipo_env == 'YML':
        formato_yml = yaml.dump(env, default_flow_style=False)
        arquivo = open(dir_env, "w")
        arquivo.write(formato_yml)
        arquivo.close()
    else:
        # converte array para arquio .env se projeto for laravel
        dir_env = open(dir_env, "w")
        for i in env:
            dir_env.write(i + format('\n'))
        dir_env.close()
    ## Fim criação de arquivos de banco de dados.

    ## Executar commandos
    for i in executar_comando:
        saida_comando = os.popen('bash '+ diretorio_home_pipeline +'/command.sh \"' + i +'\" \"'+ dir_app +'\"')
        print('Pipeline: ' + saida_comando.read())
