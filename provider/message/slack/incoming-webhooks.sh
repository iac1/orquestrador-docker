#!/usr/bin/env bash
msg="$1"
[[ -n $msg ]] || exit #msg="Nenhuma mensagem definida"
curl -X POST -s -H 'Content-type: application/json' --data "
{
	'blocks': [
		{
			'type': 'section',
			'text': {
				'type': 'mrkdwn',
				'text': '$msg'
			}
		}
	]
}" https://hooks.slack.com/services/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
# WorkSpace Slack: orq-pipeline.slack.com
# Canal de recebimentos das notificações: integração-do-pipeline-com-slack
