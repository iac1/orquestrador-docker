#!/bin/bash

# Define o diretório principal do Orq-Pipeline
HOME_DEPLOYER="/opt/orquestrador-docker"

# Verifica se orquestrador foi instalado no diretório correto.
if [ ! -d $HOME_DEPLOYER ] && [ ! -L $HOME_DEPLOYER ]; then
  echo "Orquestrador não encontrado em $HOME_DEPLOYER" ; exit ;
fi

# Definição de variáveis para execução do orquestrador
source $HOME_DEPLOYER/libs/headVariable
source $HOME_DEPLOYER/libs/setAlert

# Verifica se script está sendo executado como root
if [ $(whoami) != "root" ];then
   setAlert "warning" "Execute o orquestrador como root"
   exit
fi

# Mensageiro
setAlert "success" "Orquestrador em execução..."

# Inclusão de funções:
source $HOME_DEPLOYER/provider/registry/login
source $HOME_DEPLOYER/libs/getVariable
source $HOME_DEPLOYER/libs/getPortAvaiable
source $HOME_DEPLOYER/libs/executionParameters
source $HOME_DEPLOYER/libs/setCmdOrquestrador

# Limpa históricos de execuções dos container para liberação de espaço em disco.
docker rm $(docker ps -aq --filter "name=$PREFIX_NAME") > /dev/null 2>&1
# Limpa container sem tag (<none>) para liberação de espaço em disco.
docker rmi $(docker images -q --filter "dangling=true") > /dev/null 2>&1

# Processa todas as configurações existentes
for appFileConf in $(find  $HOME_DEPLOYER/app/conf-enabled/ -type f -name "*.conf")
do
    # Coletores de vairáveis definidas no .conf de cada aplicação em app/conf-enabled
    source $HOME_DEPLOYER/libs/defineVariable

    # Navega para o diretório deployer
    cd $HOME_DEPLOYER
    # Verifica a existência do Dockerfile

    # Verifica se Docker da aplicação já está em execução
    if [ $(docker ps | rev | awk '{print $1}' | rev | grep -x $PREFIX_NAME$CONTAINER_NAME > /dev/null 2>&1 ; echo $?) -ne 0 ];then

          setAlert "yellow" "#### Provisionando recursos para $CONTAINER_NAME"
          ## sessão de build
          source $HOME_DEPLOYER/libs/buildDockerFile
          ## fim sessão build

          # Executa o container mapeando o diretório do deployer (onde contém as chaves SSH para clone do Git) e coleta seu ID de execução
          HASH_ID=$(docker container run -di --env-file $ENV_LIST --name $PREFIX_NAME$CONTAINER_NAME -p $MAPPER_PORT_EXTERNAL:$MAPPER_PORT_INTERNAL $CONTAINER_NAME)

          # Filtra o ID coletado para 12 caracteres
          CONTAINER_ID=${HASH_ID:0:12}

          # Envia arquivo .cron para o diretório cron do container pertinente ao projeto. Conforme $CONTAINER_NAME
          docker cp $CRON_FILES/$CONTAINER_NAME $CONTAINER_ID:/etc/cron.d/ > /dev/null 2>&1

          # Executa o script de prepração da aplicação dentro do Docker
          docker container exec -it $CONTAINER_ID bash $RUN_SCRIPT

          # Linca o Apache do Docker com Apache do Host.
          source $RUN_LINK_APACHE2
          setAlert "yellow" "####"
    else
          setAlert "warning" "Docker de $CONTAINER_NAME já está em execução"
    fi

done
setAlert "success" "Fim do orquestrador."

## Arquitetura e desenvolvimento:
# Leomar Duarte (https://www.linkedin.com/in/leomar-duarte/)
# Marcelo Viana (https://www.linkedin.com/in/marcelovianaalmeida/)
