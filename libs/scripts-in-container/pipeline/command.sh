#!/usr/bin/env bash
command="$1"
dir_app="$2"
path_home_thisScript=$(dirname "$0")
status=0
command_file="/tmp/orq-pipeline-command"
command_result="/tmp/orq-pipeline-command-result"

# Pega valor por de variável de ambiente
CONTAINER_NAME=$CONTAINER_NAME
BRANCH=$BRANCH
TYPE_ENVIRONMENT=$TYPE_ENVIRONMENT
# Coleta valor do diretório de trabalho da aplicação que recebeu o pull
user_name_push=$(git log -1 --format="%an" $BRANCH)

title_app="\`\`\`Container: $CONTAINER_NAME - Branch: $BRANCH - Ambiente: $TYPE_ENVIRONMENT - $(date +'%d/%m/%Y %H:%M:%S')
Publicado por: $user_name_push\`\`\`"

> $command_file

[[ -n "$command" ]] || exit
# Navega para o diretório do app
cd $dir_app

sendMessageSlack() {
  message="$1"
  # Aguarda 1 segundo
  sleep 1
  bash $path_home_thisScript/../libs/slack/incoming-webhooks.sh "$message" > /dev/null
}

# Salva o comando em aruivo:
# Enviar o comando para arquivo envia que o comando retorne "Too many arguments, expected arguments"
echo "$command" > $command_file

# Executa o comando:
bash "$command_file" > $command_result 2>&1
if [ $? -ne 0 ];then
  status=1
  # Envia mensagem pelo Slack
  sendMessageSlack "$title_app \n> \`[ FALHA ] $command\`"
  # Retorna mensagem para o pipeline
  echo -n "[ FALHA ] $command"
else
  # Envia mensagem pelo Slack
  sendMessageSlack "$title_app \n> \`[ SUCESSO ] $command\`"
  # Retorna mensagem para o pipeline
  echo -n "[ SUCESSO ] $command"
fi
