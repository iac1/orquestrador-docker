## API Orq-Pipeline
Esta API fornece meios de interagir com os containers de modo a listar/criar/executar/parar/excluir.
Isso possibilita aplicações interagirem com a API afim de oferecer o modelo self-service DevOps.

### Requisitos:
python2.7, python-pip, Flask e flask-cors

### Instalação:

Python 2.7:
```
apt-get install python2
```
Python-pip:
```
apt-get install python-pip
```
Flask:
```
pip install flask
```
flask-cors:
```
pip install -U flask-cors
```

### Rodar:

```
cd /opt/orquestrador-docker/webservice/ && python index.py
```

### Acessando:

Home de boas vindas:
```
http://127.0.0.1:5000/
```
GET - Entrega informações dos containers em execução:
```
http://127.0.0.1:5000/get
```
POST - Interage com as configurações do Orquestrador para listar/criar/executar/parar/excluir:
```
http://127.0.0.1:5000/set
```
